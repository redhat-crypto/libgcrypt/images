<!-- vim:ts=2:sts=2:sw=2:et:tw=100
  -->
[![pipeline status](https://gitlab.com/redhat-crypto/libgcrypt/images/badges/main/pipeline.svg)](https://gitlab.com/redhat-crypto/libgcrypt/images/-/commits/main/)

# libgcrypt CI Images

This project generates a series of container images to be used for compiling and testing libgcrypt,
in order to catch and highlight potential issues as soon as possible.

The images are pushed into the [gitlab.com container registry][registry].

Building these images and caching them in the registry speeds up CI runs and increases reliability
by avoiding network issues (e.g., because a mirror is temporarily unavailable).

## Adding a New Image

To add a new image, create a new directory that starts with `libgcrypt-` and add a `Dockerfile` to
this directory. The Makefile will automatically build this new image. The name of the image will be
the folder name.

## Image Dependencies

Images can depend on each other by specifying the name of the image in a `FROM` statement in the
Dockerfile. Dependencies between the images will automatically be identified from these statements.
The Makefile will ensure that images will be built in topological order.

See `dependencies.py` for the implementation of dependency auto-detection.

## How to Rebuild an Existing Image

Push a new commit to the repository. This will re-trigger the CI pipeline, which will rebuild and
push the new image to the [container registry][registry].

## Building Images Locally

The Makefile supports building the same containers locally. Just run `make -j$(nproc) --output-sync`
in a working copy.

If you want to push the images to a registry, pass the `REGISTRY` variable. Its contents are used as
a prefix to the image names. Note that the value should not include a trailing slash. To actually
trigger pushing after the build, run `make push REGISTRY=<your-registry>`.

If you do not have `docker` locally but are using a different compatible container implementation,
e.g., podman, you can set the `DOCKER` make variable: `make -j8 --output-sync DOCKER=podman`.

## Using Images Locally

To run these images locally, e.g., to reproduce a bug, use
`registry.gitlab.com/redhat-crypto/libgcrypt/images/<image-name>`:

```
podman run --rm -it \
  registry.gitlab.com/redhat-crypto/libgcrypt/images/libgcrypt-fedora:latest \
  /bin/bash
```

Note that some of the builds may require additional flags. For example, to use clang's
LeakSanitizer, you will need `--cap-add SYS_PTRACE`. To bind-mount from your home directory on
Fedora, you will have to relax some SELinux restrictions using `--security-opt label=disable`.

[registry]: https://gitlab.com/redhat-crypto/libgcrypt/images/container_registry
