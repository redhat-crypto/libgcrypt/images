#!/usr/bin/env bash

set -euo pipefail

declare -a configure_flags=()

configure_flags+=("--host=$(gcc -dumpmachine)" "--build=$(gcc -dumpmachine)")

cd /tmp
git clone --depth 1 git://git.gnupg.org/libgpg-error.git
cd libgpg-error

./autogen.sh
./configure \
	--enable-maintainer-mode \
	--prefix=/usr/local \
	"${configure_flags[@]}"

make -j"$(nproc || echo '2')"
make install

cd /tmp
rm -rf libgpg-error
