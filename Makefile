DOCKERFILES = $(wildcard libgcrypt-*/Dockerfile)
CONTAINERS = $(DOCKERFILES:%/Dockerfile=%)
REGISTRY ?=
DOCKER ?= docker

ifneq ($(REGISTRY),)
REGISTRY_PREFIX=$(REGISTRY)/
endif

.PHONY: clean all %.push

all: $(CONTAINERS:%=%.done)

push: $(CONTAINERS:%=%.push)

dependencies.mk: $(DOCKERFILES)
	python3 dependencies.py $(CONTAINERS) > dependencies.mk

%.done: %/Dockerfile install-libgpg-error.sh
	@echo -e "\e[0Ksection_start:$$(date +%s):build_$(@:%.done=%)[collapsed=true]\r\e[0KBuilding container $(@:%.done=%)"
	install -m 0755 \
		install-libgpg-error.sh \
		"$(@:%.done=%)/install-libgpg-error.sh"
	$(DOCKER) build -t "$(@:%.done=%)" "$(@:%.done=%)"
	if [ -n "$(REGISTRY)" ]; then $(DOCKER) tag "$(@:%.done=%)" "$(REGISTRY_PREFIX)$(@:%.done=%)"; fi
	touch "$@"
	@echo -e "\e[0Ksection_end:$$(date +%s):build_$(@:%.done=%)\r\e[0K"

%.push: %.done
	@echo -e "\e[0Ksection_start:$$(date +%s):push_$(@:%.done=%)\r\e[0KPushing container $(@:%.done=%)"
	if [ -n "$(REGISTRY)" ]; then $(DOCKER) push "$(REGISTRY_PREFIX)$(@:%.push=%)"; fi
	@echo -e "\e[0Ksection_end:$$(date +%s):push_$(@:%.done=%)\r\e[0K"

clean:
	$(DOCKER) rmi $(CONTAINERS) || test $$? == 1
	rm -f $(CONTAINERS:%=%.done)

include dependencies.mk
