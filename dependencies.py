#!/usr/bin/env python3
"""
Scan the given list of folders for Dockerfiles and parse their 'FROM' lines to
build a list of dependencies and print them to stdout in a Makefile format.
"""
import collections
import re
import sys

from pathlib import Path

dependencies = collections.defaultdict(list)
from_line_regex = re.compile(r"^FROM\s+([^:]+)(?::.*)?$", re.I)

images = sys.argv[1:]

for image in images:
    dockerfile = Path(image) / "Dockerfile"
    sources = []
    for line in dockerfile.read_text().splitlines():
        if match := from_line_regex.match(line):
            sources.append(match.group(1))

    for source in sources:
        if "/" in source:
            # Let's assume this is pulled from remote
            continue
        dependencies[image].append(source)

for image, dependencies in dependencies.items():
    print(f"{image}.done: {' '.join((f'{dep}.done' for dep in dependencies))}")
